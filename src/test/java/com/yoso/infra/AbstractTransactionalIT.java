package com.yoso.infra;

import org.junit.ClassRule;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.testcontainers.containers.MySQLContainer;

@ContextConfiguration(locations = {
        "classpath:/applicationContext.xml",
})
public abstract class AbstractTransactionalIT extends AbstractTransactionalJUnit4SpringContextTests {
    @ClassRule
    public static MySQLContainer mySQLContainer = ReusableMySQLContainer.getInstance();
}
