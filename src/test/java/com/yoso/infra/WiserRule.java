package com.yoso.infra;

import org.junit.rules.ExternalResource;
import org.subethamail.wiser.Wiser;

/**
 * Wiser rule for JUnit 4.
 */
public class WiserRule extends ExternalResource {
    private static ThreadLocal<Wiser> wiserThreadLocal = new ThreadLocal<>();

    @Override
    protected void before() throws Throwable {
        super.before();

        wiserThreadLocal.set(new Wiser());

        System.setProperty("MAIL_PORT", Integer.toString(wiserThreadLocal.get().getServer().getPort()));
    }

    @Override
    protected void after() {
        super.after();

        wiserThreadLocal.get().stop();
        wiserThreadLocal.remove();
    }
}
