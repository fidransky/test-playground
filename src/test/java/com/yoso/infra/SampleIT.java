package com.yoso.infra;

import org.junit.Test;
import org.springframework.test.context.jdbc.Sql;

import static org.junit.Assert.assertEquals;

public class SampleIT extends AbstractTransactionalIT {
    @Test
    @Sql("schema.sql")
    public void testZero() {
        int count = countRowsInTable("websiteaddress");

        assertEquals(0, count);
    }

    @Test
    @Sql("schema.sql")
    @Sql("data.sql")
    public void testTwo() {
        int count = countRowsInTable("websiteaddress");

        assertEquals(2, count);
    }
}
