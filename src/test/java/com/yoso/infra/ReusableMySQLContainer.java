package com.yoso.infra;

import org.testcontainers.containers.MySQLContainer;

/**
 * Reusable {@link MySQLContainer} shared among all classes extending {@link AbstractTransactionalIT}.
 *
 * Inspired by https://www.baeldung.com/spring-boot-testcontainers-integration-test#common-configuration.
 */
public class ReusableMySQLContainer extends MySQLContainer {
    private static ReusableMySQLContainer container;

    public static ReusableMySQLContainer getInstance() {
        if (container == null) {
            container = new ReusableMySQLContainer();
        }

        return container;
    }

    private ReusableMySQLContainer() {
        super();
    }

    @Override
    public void start() {
        super.start();

        System.setProperty("JDBC_DRIVER", container.getDriverClassName());
        System.setProperty("JDBC_URL", container.getJdbcUrl());
        System.setProperty("JDBC_USERNAME", container.getUsername());
        System.setProperty("JDBC_PASSWORD", container.getPassword());
    }

    @Override
    public void stop() {
        //do nothing, JVM handles shut down
    }
}
